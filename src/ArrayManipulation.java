import java.util.Arrays;

public class ArrayManipulation {
    public static void main(String[] args) {
        

        int[] number = {5, 8, 3, 2, 7};
        String[] name = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];


        for(int i=0; i<number.length; i++){
            System.out.print(number[i]+" ");
            
        }
        System.out.print("\n_____________");

        for(String n:name){
            System.out.println();
            System.out.print(n+" ");
        }
        System.out.print("\n_____________");
        
        for(int i=0; i<values.length; i++){
            System.out.println();
            values[0] = 0.65;
            values[1] = 1.25;
            values[2] = 2.55;
            values[3] = 3.85;
            System.out.print(values[i]);
            
        }
        System.out.print("\n_____________");
        
        int sum =0;
        for(int i=0; i<number.length; i++){
            sum += number[i];     
        }
        System.out.println();
        System.out.print("sum is "+sum);
        System.out.print("\n_____________");

        double max = values[0];
        for(int i=0; i<values.length; i++){
            if(values[i]> max) max = values[i];    
        }
        System.out.println();
        System.out.print("max is "+max);
        System.out.print("\n_____________");

        System.out.println();
        String[] reversedNames = {"Alice", "Bob", "Charlie", "David"};
        for (int i = reversedNames.length - 1; i >= 0; i--){
            System.out.print(reversedNames[i]+" ");
        }
        System.out.print("\n_____________");

        System.out.println();
        Arrays.sort(number);
        System.out.println("ascending number = "+Arrays.toString(number));
        System.out.print("\n_____________");

    }
}