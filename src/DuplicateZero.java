public class DuplicateZero {
    public static void main(String[] args) {
        int[] arr = {1, 0, 2, 3, 0, 4, 5, 0};
        for (int num : arr) {
            System.out.print(num + " ");
        }
    }

    public static void duplicateZeros(int[] arr) {
        int n = arr.length;
        int[] tempArray = new int[n];
        int j = 0; 
        for (int i = 0; i < n; i++) {
            if (arr[i] == 0) {
                tempArray[j++] = 0;
                if (j < n) { 
                    tempArray[j++] = 0;
                }
            } else {
                tempArray[j++] = arr[i];
            }
        }
        for (int i = 0; i < n; i++) {
            arr[i] = tempArray[i];
        }
    }
}
